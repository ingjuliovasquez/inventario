
@extends('template.admin')

@section('content')
<!-- Start content -->
      <!-- Start right Content here -->
            <!-- ============================================================== -->

                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">PRODUCTOS</h4>
                                </div>
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-right">
                                     
                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                            
                                               <H3>Cerrar sesión</H3> 
                                            </a>
                            
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ol>
                                </div>
                            </div> <!-- end row -->
                        </div>
                        <!-- end page-title -->
                    <div class="row">
                        {{-- <div class="col-md-6 col-xl-3">
        
                            <div class="card m-b-30">
                                <img class="card-img-top img-fluid" src="assets/images/small/img-2.jpg" alt="Card image cap">
                                <div class="card-body text-md-center">
                                    <a href="">
                                    <h4 class="card-title font-26 mt-0 ">Card title</h4>
                                    </a>
                                </div>
                             
                            </div>
    
                        </div><!-- end col --> --}}

                        <div class="col-md-6 col-xl-3">
                            <a href="{{ route('admin.producto') }}">
                            <div class="card m-b-30">
                                <div class="text-md-center"><br><br>
                                
                                <i class="icon-plus fa-10x"></i>
                               
                                </div>
                                <div class="card-body text-md-center">
                                    
                                    <h4 class="card-title font-26 mt-0 ">Nuevo Producto </h4>
                                   
                                    <br>
                                </div>
                            
                            </div>
                            </a>
                        </div><!-- end col -->
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="page-title">CATEGORIAS</h4>
                        </div>
                        <div class="col-md-6 col-xl-3">
                          
                            <div class="card m-b-30">
                                <a href="{{ route('admin.potables') }}">
                                <img class="card-img-top img-fluid" src="assets/images/descarga.jpg" alt="Card image cap">
                                <div class="card-body text-md-center">
                                    
                                    <h4 class="card-title font-26 mt-0 ">Potables</h4>
                                    </a>
                                </div>
                                
                             
                             
                            </div>
                       
    
                        </div><!-- end col -->
                        <div class="col-md-6 col-xl-3">
                       
                            <div class="card m-b-30">
                                <a href="{{ route('admin.comestibles') }}">
                                <img class="card-img-top img-fluid" src="assets/images/comestibles.jpg" alt="Card image cap">
                                <div class="card-body text-md-center">
                                   
                                    <h4 class="card-title font-26 mt-0 ">Comestibles</h4>
                                    </a>
                                </div>
                                
                             
                             
                            </div>
                       
    
                        </div><!-- end col -->
                        <div class="col-md-6 col-xl-3">
                       
                            <div class="card m-b-30">
                                <a href="{{ route('admin.aseo') }}">
                                <img class="card-img-top img-fluid" src="assets/images/aseo.jpg" alt="Card image cap">
                                <div class="card-body text-md-center">
                                  
                                    <h4 class="card-title font-26 mt-0 ">Aseo</h4>
                                    </a>
                                </div>
                                
                             
                             
                            </div>
                       
    
                        </div><!-- end col -->
                    
                    </div>
                </div>
            </div>

            
            <!-- ============================================================== -->
            <!-- End Right content here -->
<!-- content -->
@endsection
@section('scripts')

        <script>
  

        </script>  

@endsection