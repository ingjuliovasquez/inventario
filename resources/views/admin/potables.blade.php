
@extends('template.admin')

@section('content')
<!-- Start content -->
      <!-- Start right Content here -->
            <!-- ============================================================== -->

                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">POTABLES</h4>
                                </div>
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-right">
                                     
                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                            
                                               <H3>Cerrar sesión</H3> 
                                            </a>
                            
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ol>
                                </div>
                            </div> <!-- end row -->
                        </div>
                        <!-- end page-title -->
                    </div>
       <!-- Start content -->
      
        <div class="container-fluid">
      
            <!-- end page-title -->
            <div class="pull-right pb-2">            
                <a href="{{ route('admin') }}" class="btn btn-outline-primary " title="Agregar Videos" ><span class="btn-label"><H3> INICIO</H3></span></a>            
                <a href="{{ route('admin.producto') }}" class="btn btn-outline-primary " title="Agregar Videos" ><span class="btn-label"><H3><i class="fa fa-plus"></i> AGREGAR PRODUCTO</H3></span></a>            

            </div>           
            <br><br>
            <div class="row">
                <div class="col-8">
                    <div class="card m-b-30">
                        <div class="card-body">

                            <h4 class="mt-0 header-title"></h4>
                          
                            <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;"  id="tablaPotables">
                              
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Potables</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div>
        <!-- container-fluid -->

         <div id="editPotables" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0" id="myModalLabel">Editar Potables</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Producto</label>
                            <div class="col-sm-10">
                                <input class="form-control " type="text" value="" id="producto">
                            </div>
                            <br><br><br>
                            <label class="col-sm-2 col-form-label">Categorias</label>
                            <div class="col-sm-10">
                            
                            <select name="categoria" id="categoria_id" class="form-control"  >
                
                            @foreach ($Categorias as $categoria)
                                <option  value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
                            @endforeach 
                        </select>
                        </div>
         
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary waves-effect waves-light EditarProducto">Guardar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
         </div><!-- /.modal -->

                </div>    
            
            <!-- ============================================================== -->
            <!-- End Right content here -->
<!-- content -->
@endsection
@section('scripts')

<script src="{{ asset('js/potables.js')}}"></script>
@endsection