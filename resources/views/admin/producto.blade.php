
@extends('template.admin')

@section('content')
<!-- Start content -->
      <!-- Start right Content here -->
            <!-- ============================================================== -->

                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">PRODUCTOS</h4>
                                </div>
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-right">
                                     
                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                            
                                               <H3>Cerrar sesión</H3> 
                                            </a>
                            
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ol>
                                </div>
                            </div> <!-- end row -->
                        </div>
                        <!-- end page-title -->
                    </div>

                    

    <div class="col-10">
        <div class="pull-right pb-2">            
            <a href="{{ route('admin') }}" class="btn btn-outline-primary " title="Agregar Videos" ><span class="btn-label"><H3> INICIO</H3></span></a>            
        </div>
        <br><br>
        <div class="card m-b-30">
            <div class="card-body ">

                <h4 class="mt-0 header-title">Nuevo Producto</h4>
                {{-- <p class="sub-title">Input masks can be used to force the
                    user to enter data conform a specific format. Unlike validation, the
                    user can't enter any other key than the ones specified by the mask.
                </p> --}}
                <br>
                <form enctype="multipart/form-data" id="edit">
                    @csrf
                <div class="row  ">
                    <div class="col-lg-6">
                        <div class="p-20">
                                  
                                <div class="form-group">
                                    <label>Producto</label>
                                    <input type="text" placeholder="" name="producto" data-mask="999-99-999-9999-9" id="producto" class="form-control ">
                                    
                              
                                </div>
                              
                                  
                             
                              
                          
                                <button type="button" class="btn btn-primary waves-effect waves-light guardar " id="guardar">
                                    Guardar
                                </button>
               

                            </div> <!-- end row -->
                            </div> <!-- end row -->
                </div> <!-- end row -->
            </form>
            </div>
       
        </div>
    </div> <!-- end col -->



                </div>    
            
            <!-- ============================================================== -->
            <!-- End Right content here -->
<!-- content -->
@endsection
@section('scripts')

<script src="{{ asset('js/producto.js')}}"></script>
@endsection