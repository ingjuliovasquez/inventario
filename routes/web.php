<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| comentario-prueba
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
//Rutas para store
Route::get('/admin', 'ProductosController@index')->name('admin');
Route::get('/admin/producto', 'ProductosController@indexProducto')->name('admin.producto');
Route::post('/admin/producto/store', 'ProductosController@storeProducto')->name('producto');

//Rutas para index
Route::get('/admin/potables', 'ProductosController@indexPotables')->name('admin.potables');
Route::get('/admin/comestibles', 'ProductosController@indexComestibles')->name('admin.comestibles');
Route::get('/admin/aseo', 'ProductosController@indexAseo')->name('admin.aseo');

//Rutas para potables
Route::get('/admin/tabla/potables', 'ProductosController@tablaPotables')->name('tabla.potables');
Route::post('/admin/producto/edit', 'ProductosController@editPotables')->name('edit.potables');
Route::post('/admin/producto/delete', 'ProductosController@deletePotables')->name('delete.potables');
//Rutas para comestibles
Route::get('/admin/tabla/comestibles', 'ProductosController@tablaComestibles')->name('tabla.comestibles');
Route::post('/admin/comestibles/edit', 'ProductosController@editComestibles')->name('edit.comestibles');
Route::post('/admin/comestibles/delete', 'ProductosController@deleteComestibles')->name('delete.comestibles');
//Rutas para aseo
Route::get('/admin/tabla/aseo', 'ProductosController@tablaAseo')->name('tabla.aseo');
Route::post('/admin/aseo/edit', 'ProductosController@editAseo')->name('edit.aseo');
Route::post('/admin/aseo/delete', 'ProductosController@deleteAseo')->name('delete.aseo');



