$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });                       

    
});



$('#guardar').on( 'click',  function () {
    var _token = $('meta[name="csrf-token"]').attr('content')
    // console.log(id);

      var prod =  $('#producto').val(); 
    Swal.fire({
    
    html: '<p align="left">Selecciona Categoria<br/>',
         
   
    input: "select",
    showCancelButton: true,
    showLoaderOnConfirm: true,
    inputOptions: {
        '1': 'Potables',
        '2': 'Comestibles',
        '3': 'Aseo'
    
      },
 
    confirmButtonColor: "#b1262b",
    confirmButtonText: "Aceptar",
    cancelButtonText: "Cancelar",
    cancelButtonColor: '#262626',
    
    //
    // allowOutsideClick: () => !Swal.isLoading(),
   
  }).then((result)=>{
    if (result.value) {
      
        $.ajax({
            type: 'POST',
            data: {
                '_token':_token,
                'valor' : result.value,
                'producto':prod

                
            },
            cache: false,
            url: route('producto'),
            dataType: "JSON",
            success : function(response) {
                if(response.status == 1){
                    Swal.showLoading();
                    Swal.fire('Producto Registrado','Acción realizada con exito','success');
                    // mensaje('Registro Eliminado','Acción realizada con exito','success')
                
                     window.location.href = route('admin');
                }else{
                    Swal.fire('Ocurrio un problema, intente nuevamente','warning')
                }
        
            }
        });
     }

  })
 
});


