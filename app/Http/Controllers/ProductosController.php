<?php

namespace App\Http\Controllers;

use App\User;
use App\Producto;
use App\Categoria;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ProductosController extends Controller
{
    //
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function indexProducto()
    {
        return view('admin.producto');
    }

    public function storeProducto(Request $request)
    {
        try {
        $producto = new Producto;
        $producto->nombre = $request->producto;
        $producto->user_id = Auth::user()->id;
        $producto->categoria_id = $request->valor;
        $producto->save();             
        DB::commit();
        // return view('panel.solicitudAdmin');
        return response()->json(['response' => 'success', 'status' => 1],200);
    } catch (\Throwable $th) {
        DB::rollback();
        return response()->json(['response' => 'success', 'status' => 2, 'error' => $th],200);
    }
    return response()->json(['response' => 'success', 'status' => 3],200);
    }

    public function indexPotables()
    {
        $Categorias = Categoria::orderBy('created_at','asc')->get();
        return view('admin.potables',compact( 'Categorias'));
       
    }
    public function indexComestibles()
    {
        $Categorias = Categoria::orderBy('created_at','asc')->get();
        return view('admin.comestibles',compact( 'Categorias'));
        
    }
    public function indexAseo()
    {
        $Categorias = Categoria::orderBy('created_at','asc')->get();
        return view('admin.aseo',compact( 'Categorias'));
        
    }


    public function tablaPotables()
    {
       
        
    
        $productos = Producto::where('categoria_id', '=', 1)->orderBy('created_at','asc')->get();
      
            
        $arr_esc = array();

        
       

        foreach ( $productos as  $producto) {
                 
           
       $acciones = '<button type="button" class="btn btn-primary btn-circle" onclick="productoEditar('.$producto->id.')" data-toggle="tooltip" data-placement="top" title="Ver">Editar</button> &nbsp; '.
       '<button type="button" class="btn btn-danger  btn-circle "  onclick="productoEliminar('.$producto->id.')" data-toggle="tooltip" data-placement="top" title="Ver">Eliminar</button> &nbsp; ';
      
        
            array_push($arr_esc, array(
                'id'=>$producto->id,
                'name'=>$producto->nombre,

            
                'boton'=>$acciones,

                
            ));

        }
      
        return response()->json(['response' => 'success', 'status' => 1, 'data' => ($arr_esc)],200);


    }

    public function editPotables(Request $request)
    {

 
        DB::beginTransaction();
        try {
            $esr = Producto::find($request->id);
            $esr->nombre = $request->producto;
            $esr->user_id = Auth::user()->id;
            $esr->categoria_id = $request->valor;
            $esr->save();

            DB::commit();
           
                return response()->json(['response' => 'success', 'status' => 1],200);
        } catch (\Throwable $th) {
            DB::rollback();
            return $th;
        }
        return response()->json(['response' => 'success', 'status' => 3],200);
    }

    public function deletePotables(Request $request)
    {

 
        DB::beginTransaction();
        try {
            $esr = Producto::find($request->id);
                     
            $esr->delete();

            DB::commit();
           
             return response()->json(['response' => 'success', 'status' => 1],200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['response' => 'success', 'status' => 2, 'error' => $th],200);
        }
        return response()->json(['response' => 'success', 'status' => 3],200);  
    }


    public function tablaComestibles()
    {
       
        
    
        $productos = Producto::where('categoria_id', '=', 2)->orderBy('created_at','asc')->get();
      
            
        $arr_esc = array();

        
       

        foreach ( $productos as  $producto) {
                 
           
       $acciones = '<button type="button" class="btn btn-primary btn-circle" onclick="productoEditar('.$producto->id.')" data-toggle="tooltip" data-placement="top" title="Ver">Editar</button> &nbsp; '.
       '<button type="button" class="btn btn-danger  btn-circle "  onclick="productoEliminar('.$producto->id.')" data-toggle="tooltip" data-placement="top" title="Ver">Eliminar</button> &nbsp; ';
      
        
            array_push($arr_esc, array(
                'id'=>$producto->id,
                'name'=>$producto->nombre,

            
                'boton'=>$acciones,

                
            ));

        }
      
        return response()->json(['response' => 'success', 'status' => 1, 'data' => ($arr_esc)],200);


    }

    public function editComestibles(Request $request)
    {

 
        DB::beginTransaction();
        try {
            $esr = Producto::find($request->id);
            $esr->nombre = $request->producto;
            $esr->user_id = Auth::user()->id;
            $esr->categoria_id = $request->valor;
            $esr->save();

            DB::commit();
           
                return response()->json(['response' => 'success', 'status' => 1],200);
        } catch (\Throwable $th) {
            DB::rollback();
            return $th;
        }
        return response()->json(['response' => 'success', 'status' => 3],200);
    }

    public function deleteComestibles(Request $request)
    {

 
        DB::beginTransaction();
        try {
            $esr = Producto::find($request->id);
                     
            $esr->delete();

            DB::commit();
           
             return response()->json(['response' => 'success', 'status' => 1],200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['response' => 'success', 'status' => 2, 'error' => $th],200);
        }
        return response()->json(['response' => 'success', 'status' => 3],200);  
    }



    public function tablaAseo()
    {
       
        
    
        $productos = Producto::where('categoria_id', '=', 3)->orderBy('created_at','asc')->get();
      
            
        $arr_esc = array();

        
       

        foreach ( $productos as  $producto) {
                 
           
       $acciones = '<button type="button" class="btn btn-primary btn-circle" onclick="productoEditar('.$producto->id.')" data-toggle="tooltip" data-placement="top" title="Ver">Editar</button> &nbsp; '.
       '<button type="button" class="btn btn-danger  btn-circle "  onclick="productoEliminar('.$producto->id.')" data-toggle="tooltip" data-placement="top" title="Ver">Eliminar</button> &nbsp; ';
      
        
            array_push($arr_esc, array(
                'id'=>$producto->id,
                'name'=>$producto->nombre,

            
                'boton'=>$acciones,

                
            ));

        }
      
        return response()->json(['response' => 'success', 'status' => 1, 'data' => ($arr_esc)],200);


    }

    public function editAseo(Request $request)
    {

 
        DB::beginTransaction();
        try {
            $esr = Producto::find($request->id);
            $esr->nombre = $request->producto;
            $esr->user_id = Auth::user()->id;
            $esr->categoria_id = $request->valor;
            $esr->save();

            DB::commit();
           
                return response()->json(['response' => 'success', 'status' => 1],200);
        } catch (\Throwable $th) {
            DB::rollback();
            return $th;
        }
        return response()->json(['response' => 'success', 'status' => 3],200);
    }

    public function deleteAseo(Request $request)
    {

 
        DB::beginTransaction();
        try {
            $esr = Producto::find($request->id);
                     
            $esr->delete();

            DB::commit();
           
             return response()->json(['response' => 'success', 'status' => 1],200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['response' => 'success', 'status' => 2, 'error' => $th],200);
        }
        return response()->json(['response' => 'success', 'status' => 3],200);  
    }




}
