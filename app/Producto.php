<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    //
    public $table = 'productos';
    public $timestamps = false;
    public $fillable = [
        'id',
        'nombre',
        'user_id',
        'categoria_id'
        
       
    ];

    public function categoria(){
        return $this->belongsTo('App\Categoria', 'categoria_id', 'id');
    }


    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

}
