<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //
    public $table = 'categorias';
    public $timestamps = false;
    public $fillable = [
        'id',
        'nombre'
        
       
    ];

    public function producto(){
        return $this->HasMany('App\Producto', 'categoria_id', 'id');
    }
}
